import { createApp } from 'vue'
import App from './App.vue'

window.Office.onReady(() => {
    try {
     createApp(App).mount('#app');
    } catch (error) {
    // Note: In a production add-in, you'd want to notify the user through your add-in's UI.
       console.error("To fix:" + error);
    }

});