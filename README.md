# excel_addin

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### copy the manifest to the excel addin folder
 cp  src/excel/titan_mapping/titan_mapping/manifest.xml /Users/akivaelkayam/Library/Containers/com.microsoft.Excel/Data/Documents/wef/manifest_titan_mapping.xml